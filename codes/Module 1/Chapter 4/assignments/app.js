function ClickFunc(op) {
    var num1 = parseFloat(document.getElementById("Num1").value);
    var num2 = parseFloat(document.getElementById("Num2").value);
    var value = '0';
    switch (op) {
        case '+':
            value = (num1 + num2).toString();
            break;
        case '-':
            value = (num1 - num2).toString();
            break;
        case '*':
            value = (num1 * num2).toString();
            break;
        case '/':
            if (num2 != 0)
                value = (num1 / num2).toString();
            else
                value = "Infinite";
            break;
        default:
            value = "Error";
    }
    document.getElementById("Ans").value = value;
}
//# sourceMappingURL=app.js.map