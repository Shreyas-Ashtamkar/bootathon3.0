
//Declared a function for easy getting value from document
function getVal(id:string){
    return parseFloat((<HTMLInputElement>document.getElementById(id)).value)
}

function area(x1:number, y1:number, x2:number, y2:number, x3:number, y3:number){
    
    //Mathematical Formula to Calculate the Area
    var triArea = (x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) / 2.0;

    return Math.abs(triArea);
}

function Check() {
    //Input Triangle Points
    var X1 = getVal("X1"), Y1 = getVal("Y1");
    var X2 = getVal("X2"), Y2 = getVal("Y2");
    var X3 = getVal("X3"), Y3 = getVal("Y3");

    //Input Points to Check
    var X  = getVal("X"),  Y  = getVal("Y");
    
    //Total area of The Triangle
    var totalArea = area(X1, Y1, X2, Y2, X3, Y3 );
    
    //We can consider input point seperates 3 areas when vertices join the dot.
    var partArea1 = area(X , Y , X2, Y2, X3, Y3 );
    var partArea2 = area(X1, Y1, X , Y , X3, Y3 );
    var partArea3 = area(X1, Y1, X2, Y2, X , Y  );

    //Taking the Sum of All Partial Areas
    var sumPartArea = partArea1 + partArea2 + partArea3;
    
    // By Default Show an Error  ( Initialised value as an error Message )
    var value = 'Cannot Check, please check Inputs.';

    // Value can be Nan
    if (totalArea != NaN)
        // If all three points are same
        if (totalArea == 0){
            //Check if the Input point is also the Same --> Means Inside
            if (X1 == X && Y1 == Y) value = 'Inside';
            else value = 'Outside';
        }
        // If Area of Triangle is Not Zero, check if it is equal to the sum of partial Areas
        else if (totalArea == sumPartArea){
            value = 'Inside';
        }
        else value = 'Outside';

    
    // Display the Answer in HTML
    (<HTMLInputElement>document.getElementById("Ans")).value = value;

}
