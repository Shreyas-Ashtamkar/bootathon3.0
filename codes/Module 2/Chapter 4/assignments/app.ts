// Keep a track of Last given Number
var pastNo = 0;

//Funtion that gets clled when Button is clicked.
function generateTables(){
    //HTML Table Element to Add Resultant Rows
    var table: HTMLTableElement = <HTMLTableElement> document.getElementById("Output Table");
    
    var no = parseInt((<HTMLInputElement>document.getElementById("num")).value);
    var row, cell;
    

    //Delete all the rows of the previous Session
    for (let i = 0; i < pastNo; i++) {
        table.deleteRow(-1);    
    }

    //dynamically generate rows as per required
    for (let i = 1; i <= no; i++) {
        row = table.insertRow();

        cell = row.insertCell();
        cell.innerHTML = no.toString();

        cell = row.insertCell();
        cell.innerHTML = "X";

        cell = row.insertCell();
        cell.innerHTML = i;

        cell = row.insertCell();
        cell.innerHTML = " = ";

        cell = row.insertCell();
        cell.innerHTML = (no*i).toString();
    }

    //After everything done, Store this Current Nunber as past
    pastNo = no;
}