var totForces = 2;
var lastrow = 3;
var inputTable;
var row;
var cell;
var forceNo = '0';
function addForce() {
    inputTable = document.getElementById("InputTable");
    totForces += 1; // For Keeping Track of Force Number
    lastrow += 1; // For keeping track of keeping the Row Number
    forceNo = totForces.toString();
    row = inputTable.insertRow(lastrow);
    // row.id = `F${forceNo}`;
    // row.innerHTML = `
    // <th><h2>Force ${forceNo} -----> </h2></th>
    // <td><input type="text" name="" id="F${forceNo}" placeholder="Magnitude"/></td>
    // <td><input type="number" name="" id="A${forceNo}" placeholder="Angle" min="0" max="359.99" step="0.01"/></td>
    // <td style="border: none; width: 60px;"><input type="button" value="-" class="rowButton" id="${forceNo}" title="Remove Force"></td>`
    cell = row.insertCell();
    cell.style.border = "none";
    cell.innerHTML = `<th><h2>Force ${forceNo} -----> </h2></th>`;
    cell = row.insertCell();
    cell.innerHTML = `<input type="text" name="" id="F${forceNo}" placeholder="Magnitude"/>`;
    cell = row.insertCell();
    cell.innerHTML = `<input type="number" name="" id="A${forceNo}" placeholder="Angle" min="0" max="359.99" step="0.01"/>`;
    // cell = row.insertCell();
    // cell.style.border = "none";
    // cell.style.width  = "60px"
    // cell.innerHTML =`<input type="button" value="-" class="rowButton" id="${forceNo}" title="Remove Force" onclick=removeForce(${(totForces+1).toString()})>`
}
function removeForce() {
    if (lastrow > 3) {
        inputTable = document.getElementById("InputTable");
        inputTable.deleteRow(lastrow);
        console.log("Deleted Last Row");
        lastrow -= 1;
        totForces -= 1;
    }
}
function Calculate() {
    /*
        F1 * cos(A1) = F1x,
        F2 * cos(A2) = F2x,
        F3 * cos(A3) = F3x,
        F4 * cos(A4) = F4x;

        F1 * sin(A1) = F1y,
        F2 * sin(A2) = F2y,
        F3 * sin(A3) = F3y,
        F4 * sin(A4) = F4y;

        F1x + F2x + F3x + F4x = Rx;
        F1y + F2y + F3y + F4y = Ry;
        tan(Ry / Rx) = RA;
    Rx * Rx + Ry * Ry = R ^ 2;
    */
    var forceMag = 0;
    var forceAng = 0;
    var resX = 0;
    var resY = 0;
    var res = 0;
    var resAng = 0;
    for (let i = 1; i <= totForces; i++) {
        forceMag = getVal(`F${i.toString()}`);
        forceAng = getVal(`A${i.toString()}`) * Math.PI / 180;
        resX += forceMag * Math.cos(forceAng);
        resY += forceMag * Math.sin(forceAng);
    }
    res = Math.sqrt(resX * resX + resY * resY);
    resAng = Math.atan(resY / resX) * 180 / Math.PI;
    document.getElementById("resMag").innerHTML = res.toPrecision(6);
    document.getElementById("resAng").innerHTML = resAng.toPrecision(3);
    console.log(`result Calculated = ${res.toPrecision(6)},  @  ${resAng.toPrecision(3)}`);
    if (res != NaN && resAng != NaN)
        document.getElementById("Output-Table").style.visibility = "visible";
    else
        document.getElementById("Output-Table").style.visibility = "hidden";
}
//Declared a function for easy getting value from document
function getVal(id) {
    return parseFloat(document.getElementById(id).value);
}
//# sourceMappingURL=app.js.map