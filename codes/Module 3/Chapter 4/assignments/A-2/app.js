var datapoints1 = [];
var datapoints2 = [];
function draw() {
    for (let i = 0; i < 10; i++) {
        datapoints1.push({ x: i, y: i * i });
        datapoints2.push({ x: i, y: i * i + i });
    }
    drawgraph("MyCanvas", datapoints1, datapoints2, "X-axis", "Y-axis", "comparison", "X^2", "X^2+X");
}
//# sourceMappingURL=app.js.map