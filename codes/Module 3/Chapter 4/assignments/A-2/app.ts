// declare var drawgraph;
// declare var drawgraph2;

var datapoints1:{ x:number, y:number}[] = [];
var datapoints2:{ x:number, y:number}[] = [];

function draw(){

    for (let i = 0; i<10; i++){
        datapoints1.push({x:i, y:i*i});
        datapoints2.push({x:i, y:i*i+i});
    }

    drawgraph2("MyCanvas", datapoints1 ,datapoints2, "X-axis", "Y-axis", "comparison", "X^2", "X^2+X");
}