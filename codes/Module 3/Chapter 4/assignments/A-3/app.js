class LogicGate {
    constructor(n) {
        this.Name = '';
        this.Name = n;
    }
    draw() { }
}
class AndGate extends LogicGate {
    constructor() {
        super("AND");
    }
    draw(startX = 100, startY = 100) {
        startX = parseFloat(document.getElementById("posX").value);
        startY = parseFloat(document.getElementById("posY").value);
        let canvas = document.getElementById('mycanvas');
        let context = canvas.getContext("2d");
        context.strokeStyle = 'white';
        this.canvas = canvas;
        this.context = context;
        this.clearCanvas();
        context.beginPath();
        context.moveTo(startX, startY);
        context.lineTo(startX + 100, startY);
        context.arc(startX + 100, startY + 50, 50, 3 * Math.PI / 2, Math.PI / 2);
        context.lineTo(startX, startY + 100);
        context.lineTo(startX, startY);
        context.stroke();
        context.closePath();
    }
    clearCanvas() {
        // this.context.fillStyle = "#FFFFFF";
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }
}
class ORGate extends LogicGate {
    constructor() {
        super("OR");
    }
    draw(startX = 100, startY = 100) {
        startX = parseFloat(document.getElementById("posX").value);
        startY = parseFloat(document.getElementById("posY").value);
        let canvas = document.getElementById('mycanvas');
        let context = canvas.getContext("2d");
        context.strokeStyle = 'white';
        this.canvas = canvas;
        this.context = context;
        this.clearCanvas();
        context.beginPath();
        context.moveTo(startX, startY);
        context.lineTo(startX + 100, startY);
        context.arc(startX + 100, startY + 50, 50, 3 * Math.PI / 2, Math.PI / 2);
        context.lineTo(startX, startY + 100);
        context.arc(startX, startY + 50, 50, Math.PI / 2, 3 * Math.PI / 2, true);
        context.stroke();
        context.closePath();
    }
    clearCanvas() {
        // this.context.fillStyle = "#FFFFFF";
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }
}
class NOTGate extends LogicGate {
    constructor() {
        super("NOT");
    }
    draw(startX = 100, startY = 100) {
        startX = parseFloat(document.getElementById("posX").value);
        startY = parseFloat(document.getElementById("posY").value);
        let canvas = document.getElementById('mycanvas');
        let context = canvas.getContext("2d");
        context.strokeStyle = 'white';
        this.canvas = canvas;
        this.context = context;
        this.clearCanvas();
        context.beginPath();
        context.moveTo(startX, startY);
        // context.lineTo(startX+100,startY);
        context.lineTo(startX + 150, startY + 50);
        context.lineTo(startX, startY + 100);
        // context.lineTo(startX,startY+100);
        context.lineTo(startX, startY);
        context.stroke();
        context.closePath();
    }
    clearCanvas() {
        // this.context.fillStyle = "#FFFFFF";
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }
}
function clickfun() {
    var a;
    var opt = document.getElementById('mygateselector').value;
    switch (opt) {
        case 'AND':
            a = new AndGate();
            break;
        case 'OR':
            a = new ORGate();
            break;
        case 'NOT':
            a = new NOTGate();
            break;
        default:
            break;
    }
    a.draw();
}
//# sourceMappingURL=app.js.map