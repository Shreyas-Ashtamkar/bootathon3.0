abstract class LogicGate
{
    Name:string = '';

    constructor(n:string){
        this.Name = n;
    }

    protected canvas: HTMLCanvasElement;
    protected context: CanvasRenderingContext2D;

    draw(){ }
}

class AndGate extends LogicGate
{    
    constructor(){
        super("AND");
    }

    public draw( startX:number = 100, startY:number = 100){
        startX = parseFloat((<HTMLInputElement> document.getElementById("posX")).value)
        startY = parseFloat((<HTMLInputElement> document.getElementById("posY")).value)
        let canvas  = <HTMLCanvasElement> document.getElementById('mycanvas');
        let context = canvas.getContext("2d");

        context.strokeStyle = 'white';

        this.canvas = canvas;
        this.context = context;
    
        this.clearCanvas();

        context.beginPath();
        
        context.moveTo(startX,startY);
        context.lineTo(startX+100,startY);
        
        context.arc(startX+100,startY+50,50,3*Math.PI/2,Math.PI/2);

        context.lineTo(startX,startY+100);
        context.lineTo(startX,startY)

        context.stroke();
        context.closePath();
    }

    public clearCanvas() {
        // this.context.fillStyle = "#FFFFFF";
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }
}

class ORGate extends LogicGate
{    
    constructor(){
        super("OR");
    }

    public draw( startX:number = 100, startY:number = 100){
        startX = parseFloat((<HTMLInputElement> document.getElementById("posX")).value)
        startY = parseFloat((<HTMLInputElement> document.getElementById("posY")).value)
        let canvas  = <HTMLCanvasElement> document.getElementById('mycanvas');
        let context = canvas.getContext("2d");

        context.strokeStyle = 'white';

        this.canvas = canvas;
        this.context = context;
    
        this.clearCanvas();

        context.beginPath();
        
        context.moveTo(startX,startY);
        context.lineTo(startX+100,startY);
        
        context.arc(startX+100,startY+50,50,3*Math.PI/2,Math.PI/2);

        context.lineTo(startX,startY+100);

        context.arc(startX,startY+50,50,Math.PI/2,3*Math.PI/2, true);

        context.stroke();
        context.closePath();
    }

    public clearCanvas() {
        // this.context.fillStyle = "#FFFFFF";
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }
}

class NOTGate extends LogicGate
{    
    constructor(){
        super("NOT");
    }

    public draw( startX:number = 100, startY:number = 100){
        startX = parseFloat((<HTMLInputElement> document.getElementById("posX")).value)
        startY = parseFloat((<HTMLInputElement> document.getElementById("posY")).value)
        let canvas  = <HTMLCanvasElement> document.getElementById('mycanvas');
        let context = canvas.getContext("2d");

        context.strokeStyle = 'white';

        this.canvas = canvas;
        this.context = context;
    
        this.clearCanvas();

        context.beginPath();
        
        context.moveTo(startX,startY);
        // context.lineTo(startX+100,startY);

        context.lineTo(startX+150,startY+50);
        context.lineTo(startX,startY+100);
        
        // context.lineTo(startX,startY+100);
        context.lineTo(startX,startY)

        context.stroke();
        context.closePath();
    }

    public clearCanvas() {
        // this.context.fillStyle = "#FFFFFF";
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }
}

function clickfun(){
    var a:LogicGate;

    var opt = (<HTMLSelectElement>document.getElementById('mygateselector')).value;

    switch (opt) {
        case 'AND':
            a = new AndGate();
            break;
        
        case 'OR':
            a = new ORGate();
            break;
        
        case 'NOT':
            a = new NOTGate();
            break;
    
        default:
            break;
    }

    a.draw();
}